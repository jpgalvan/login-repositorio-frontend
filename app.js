const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const cors = require('cors');


const app = express();

const usuario = require('./routes/usuario');

//const port = 3000;
const port = process.env.PORT || 8080;
app.use(cors());

app.use(express.static(path.join(__dirname, 'public')));

app.use(bodyParser.json());

app.use('/usuario', usuario);


app.get('/', (req, res) => {
    res.send('Invalid endpoint');
});

app.listen(port, () => {
    console.log("Server started on port: " + port)
});