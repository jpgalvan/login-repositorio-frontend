import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  usuarioForm: FormGroup;
  constructor(public fb: FormBuilder, public authService: AuthService, public router:Router) { }

  ngOnInit() {
    this.usuarioForm = this.fb.group({
      usuario: ['', Validators.required],
      password: ['', Validators.required]
    });
    localStorage.clear();
  }

  authUser(){
    this.authService.auth(this.usuarioForm.value).subscribe(
      (res) =>{
        this.authService.storeUserinfo(res);
        this.router.navigate(['home']);
      },
      (err) =>{
        alert('Usuario o contraseña incorrectos');
      }
    );
  }

}
